package freelance.example.common;

/**
 * Created by Seryozha on 20/1/2017.
 */
public class Question {

    private int id;

    private String text;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
