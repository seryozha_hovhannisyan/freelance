package freelance.example.common.exception;

/**
 * Created by Seryozha on 20/1/2017.
 */
public class DatabaseException extends Exception{

    public DatabaseException() {
    }

    public DatabaseException(String message) {
        super(message);
    }

    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseException(Throwable cause) {
        super(cause);
    }
}
