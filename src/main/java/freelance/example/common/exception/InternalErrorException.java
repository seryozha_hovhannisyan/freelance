package freelance.example.common.exception;

/**
 * Created by Seryozha on 20/1/2017.
 */
public class InternalErrorException extends Exception{

    public InternalErrorException() {
    }

    public InternalErrorException(String message) {
        super(message);
    }

    public InternalErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public InternalErrorException(Throwable cause) {
        super(cause);
    }
}
