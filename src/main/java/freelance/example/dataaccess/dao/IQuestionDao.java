package freelance.example.dataaccess.dao;

import freelance.example.common.Question;
import freelance.example.common.exception.DatabaseException;

import java.util.List;

/**
 * Created by Seryozha on 20/1/2017.
 */
public interface IQuestionDao {

    public void add(List<Question> questions) throws DatabaseException;

    public List<Question> getAll() throws DatabaseException;
}
