package freelance.example.dataaccess.dao;

import freelance.example.common.Answer;
import freelance.example.common.exception.DatabaseException;

import java.util.List;

/**
 * Created by Seryozha on 20/1/2017.
 */
public interface IAnswerDao {

    public void add(List<Answer> answers) throws DatabaseException;
    public List<Answer> getUserAnswers(int userId) throws DatabaseException;
}
