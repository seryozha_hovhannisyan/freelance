package freelance.example.dataaccess.dao.impl;

import freelance.example.common.User;
import freelance.example.common.exception.DatabaseException;
import freelance.example.common.exception.UserNotFoundException;
import freelance.example.dataaccess.dao.IUserDao;
import freelance.example.dataaccess.mapper.namespace.UserMap;

/**
 * Created by Seryozha on 20/1/2017.
 */
public class UserDao implements IUserDao {

    private UserMap userMap;

    public void setUserMap(UserMap userMap) {
        this.userMap = userMap;
    }

    @Override
    public User getByID(int userID) throws DatabaseException, UserNotFoundException {
        User user;
        try {
            user = userMap.getByID(userID);
        } catch (RuntimeException e){
            throw new DatabaseException(e);
        }

        if (user == null)
            throw new UserNotFoundException(String.format("User with %s Id not found", userID));
        return user;
    }

    @Override
    public User getUserByEmail(String email) throws DatabaseException, UserNotFoundException {
        User user;
        try {
            user = userMap.getUserByEmail(email);
        } catch (RuntimeException e){
            throw new DatabaseException(e);
        }

        if (user == null)
            throw new UserNotFoundException(String.format("User with %s email not found", email));
        return user;
    }

    @Override
    public void addUser(User user) throws DatabaseException {
        try {
            userMap.add(user);
        } catch (RuntimeException e){
            throw new DatabaseException(e);
        }
    }
}
