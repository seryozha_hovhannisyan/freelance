package freelance.example.dataaccess.dao.impl;

import freelance.example.common.Question;
import freelance.example.common.exception.DatabaseException;
import freelance.example.dataaccess.dao.IQuestionDao;
import freelance.example.dataaccess.mapper.namespace.QuestionMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Seryozha on 20/1/2017.
 */
public class QuestionDao implements IQuestionDao {

    private QuestionMap questionMap;

    public void setQuestionMap(QuestionMap questionMap) {
        this.questionMap = questionMap;
    }

    @Override
    public void add(List<Question> questions) throws DatabaseException {
        try {
            Map<String, List<Question>> map = new HashMap<String, List<Question>>();
            map.put("questions", questions);
            questionMap.add(map);
        } catch (RuntimeException e){
            throw new DatabaseException(e);
        }
    }

    @Override
    public List<Question> getAll() throws DatabaseException {
        try {
            return questionMap.getAll();
        } catch (RuntimeException e){
            throw new DatabaseException(e);
        }
    }
}
