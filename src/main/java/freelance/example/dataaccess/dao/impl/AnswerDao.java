package freelance.example.dataaccess.dao.impl;

import freelance.example.common.Answer;
import freelance.example.common.Question;
import freelance.example.common.exception.DatabaseException;
import freelance.example.dataaccess.dao.IAnswerDao;
import freelance.example.dataaccess.mapper.namespace.AnswerMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Seryozha on 20/1/2017.
 */
public class AnswerDao implements IAnswerDao {

    private AnswerMap answerMap;

    public void setAnswerMap(AnswerMap answerMap) {
        this.answerMap = answerMap;
    }

    @Override
    public void add(List<Answer> answers) throws DatabaseException{
        try {
            Map<String, List<Answer>> map = new HashMap<String, List<Answer>>();
            map.put("answers", answers);
            answerMap.add(map);
        } catch (RuntimeException e){
            throw new DatabaseException(e);
        }
    }

    @Override
    public List<Answer> getUserAnswers(int userId) throws DatabaseException {
        try {
            return answerMap.getByUser(userId);
        } catch (RuntimeException e){
            throw new DatabaseException(e);
        }
    }
}
