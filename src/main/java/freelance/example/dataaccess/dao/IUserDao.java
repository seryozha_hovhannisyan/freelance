package freelance.example.dataaccess.dao;

import freelance.example.common.User;
import freelance.example.common.exception.DatabaseException;
import freelance.example.common.exception.UserNotFoundException;


/**
 * Created by Seryozha on 20/1/2017.
 */
public interface IUserDao {

    public User getByID(int userID) throws DatabaseException, UserNotFoundException;

    public User getUserByEmail(String email) throws DatabaseException, UserNotFoundException;

    public void addUser(User user) throws DatabaseException;

}
