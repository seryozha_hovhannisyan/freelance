package freelance.example.dataaccess.mapper.namespace;

import freelance.example.common.User;

/**
 * Created by Seryozha on 20/1/2017.
 */
public interface UserMap {

    public User getByID(int userID);

    public User getUserByEmail(String email);

    public void add(User user);
}
