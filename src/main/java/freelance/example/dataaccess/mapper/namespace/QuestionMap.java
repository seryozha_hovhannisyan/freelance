package freelance.example.dataaccess.mapper.namespace;

import freelance.example.common.Question;

import java.util.List;
import java.util.Map;

/**
 * Created by Seryozha on 20/1/2017.
 */
public interface QuestionMap {

    public void add(Map map);

    public List<Question> getAll();
}
