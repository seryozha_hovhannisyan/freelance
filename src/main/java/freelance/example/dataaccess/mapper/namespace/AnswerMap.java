package freelance.example.dataaccess.mapper.namespace;

import freelance.example.common.Answer;

import java.util.List;
import java.util.Map;

/**
 * Created by Seryozha on 20/1/2017.
 */
public interface AnswerMap {

    void add(Map map);

    List<Answer> getByUser(int id);
}
