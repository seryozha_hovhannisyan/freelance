package freelance.example.manager;

import freelance.example.common.Answer;
import freelance.example.common.exception.InternalErrorException;

import java.util.List;

/**
 * Created by Seryozha on 20/1/2017.
 */
public interface IAnswerManager {

    public void add(List<Answer> answers) throws InternalErrorException;
    public List<Answer> getUserAnswers(int userID) throws InternalErrorException;
}
