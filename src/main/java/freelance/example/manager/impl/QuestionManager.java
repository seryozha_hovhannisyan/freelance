package freelance.example.manager.impl;

import freelance.example.common.Question;
import freelance.example.common.exception.DatabaseException;
import freelance.example.common.exception.InternalErrorException;
import freelance.example.dataaccess.dao.IQuestionDao;
import freelance.example.manager.IQuestionManager;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Seryozha on 20/1/2017.
 */
@Transactional(readOnly = true)
public class QuestionManager implements IQuestionManager {

    private IQuestionDao questionDao;

    public void setQuestionDao(IQuestionDao questionDao) {
        this.questionDao = questionDao;
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void add(List<Question> questions) throws InternalErrorException {
        try {
            questionDao.add(questions);
        } catch (DatabaseException e) {
            throw new InternalErrorException(e);
        }
    }

    @Override
    public List<Question> getAll() throws InternalErrorException {
        try {
            return questionDao.getAll();
        } catch (DatabaseException e) {
            throw new InternalErrorException(e);
        }
    }
}
