package freelance.example.manager.impl;

import freelance.example.common.User;
import freelance.example.common.exception.DatabaseException;
import freelance.example.common.exception.InternalErrorException;
import freelance.example.common.exception.UserNotFoundException;
import freelance.example.dataaccess.dao.IUserDao;
import freelance.example.manager.IUserManager;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Seryozha on 20/1/2017.
 */
@Transactional(readOnly = true)
public class UserManager implements IUserManager {

    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }

    private IUserDao userDao;

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addUser(User user) throws InternalErrorException {
        try {
            userDao.addUser(user);
        } catch (DatabaseException e) {
            throw new InternalErrorException(e);
        }
    }

    @Override
    public User getUser(int userId) throws InternalErrorException, UserNotFoundException {
        try {
            return userDao.getByID(userId);
        } catch (DatabaseException e) {
            throw new InternalErrorException(e);
        }
    }

    @Override
    public User getUserByEmail(String email) throws InternalErrorException, UserNotFoundException {
        try {
            return userDao.getUserByEmail(email);
        } catch (DatabaseException e) {
            throw new InternalErrorException(e);
        }
    }
}
