package freelance.example.manager.impl;

import freelance.example.common.Answer;
import freelance.example.common.exception.DatabaseException;
import freelance.example.common.exception.InternalErrorException;
import freelance.example.dataaccess.dao.IAnswerDao;
import freelance.example.manager.IAnswerManager;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Seryozha on 20/1/2017.
 */
@Transactional(readOnly = true)
public class AnswerManager implements IAnswerManager {

    private IAnswerDao answerDao;

    public void setAnswerDao(IAnswerDao answerDao) {
        this.answerDao = answerDao;
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void add(List<Answer> answers) throws InternalErrorException {
        try {
            answerDao.add(answers);
        } catch (DatabaseException e) {
            throw new InternalErrorException(e);
        }
    }

    @Override
    public List<Answer> getUserAnswers(int userID) throws InternalErrorException {
        try {
            return answerDao.getUserAnswers(userID);
        } catch (DatabaseException e) {
            throw new InternalErrorException(e);
        }
    }
}
