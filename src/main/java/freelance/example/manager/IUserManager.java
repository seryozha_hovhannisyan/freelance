package freelance.example.manager;

import freelance.example.common.User;
import freelance.example.common.exception.InternalErrorException;
import freelance.example.common.exception.UserNotFoundException;

/**
 * Created by Seryozha on 20/1/2017.
 */
public interface IUserManager {

    public void addUser(User user) throws InternalErrorException;

    public User getUser(int userId) throws InternalErrorException, UserNotFoundException;
    public User getUserByEmail(String email) throws InternalErrorException, UserNotFoundException;
}
