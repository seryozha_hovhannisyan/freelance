package freelance.example.manager;

import freelance.example.common.Question;
import freelance.example.common.exception.InternalErrorException;

import java.util.List;

/**
 * Created by Seryozha on 20/1/2017.
 */
public interface IQuestionManager {

    public void add(List<Question> questions) throws InternalErrorException;

    public List<Question> getAll() throws InternalErrorException;


}
