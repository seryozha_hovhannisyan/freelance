package freelance.example.action;

import freelance.example.common.Answer;
import freelance.example.common.Question;
import freelance.example.common.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Seryozha on 20/1/2017.
 */
public class DefaultDataUtil {

    /*
    1. How many years of experience of Java and Spring, Hibernate?

    2. Do you have a full time or part time job.

    3. What time of the day / night you are available to do our work
    (per the time zone of your country).
    So how many hours during week days - Mon to Friday?
    So how many hours during week ends - Saturday and Sunday?

    4. Any thing good about your self that you like to share...

    5. Your cv / resume.

    6. Example HTML5 / JSP, Spring, MY Sql app or Hello World that you did recently.
    (It is important either you provide a real working url / reference to the actual work that you did or provide any example /
     HelloWorld example with all the pieces working along with complete instructions for a non programmer to be able to compile
     and deploy on Eclipse / Maven command line, tomcat, mySql).
     */

    final static String[] answersString = new String[]{
        "Over 4 years, I have done a lot of project in java using different technologies of spring, Spring IOC, Spring batch, Spring Integration, Spring MVC. I have used hibernate as well but the last project with hibernate I have done  years ago. But I used other ORMs such as Ibatis, Mybatis, JPA, EclipseLink",
        "No I have not full time, but I can provide not less  then 4 hour per day.",
        "I am from Armenia, Timezone is GMT +4, 20-25, I prefer don't work on Saturday and Sunday , but  depends on deadline I can work extra  (0-18) ",
        "Able to work effectively alone as well as in a team. Ability to adapt easily to the new environment.",
        "Attached",
        "You are already see it :)"
    };


    public static List<Question> generateQuestions(){
        List<Question>  questions = new ArrayList<Question>();
        //1
        Question question = new Question();
        question.setText("How many years of experience of Java and Spring, Hibernate?");
        questions.add(question);
        //2
        question = new Question();
        question.setText("Do you have a full time or part time job.");
        questions.add(question);
        //3
        question = new Question();
        question.setText(" What time of the day / night you are available to do our work\n" +
                "    (per the time zone of your country).\n" +
                "    So how many hours during week days - Mon to Friday?\n" +
                "    So how many hours during week ends - Saturday and Sunday?");
        questions.add(question);
        //4
        question = new Question();
        question.setText("Any thing good about your self that you like to share...?");
        questions.add(question);
        //5
        question = new Question();
        question.setText("Your cv / resume. ");
        questions.add(question);
        //6
        question = new Question();
        question.setText("Example HTML5 / JSP, Spring, MY Sql app or Hello World that you did recently.\n" +
                "    (It is important either you provide a real working url / reference to the actual work that you did or provide any example /\n" +
                "     HelloWorld example with all the pieces working along with complete instructions for a non programmer to be able to compile \n" +
                "     and deploy on Eclipse / Maven command line, tomcat, mySql)");
        questions.add(question);

        return questions;
    }

    public static List<Answer> generateAnswers(User user, List<Question> questions) {
        List<Answer> answers = new ArrayList<Answer>();
        Answer answer;
        int index = 0;
        for (Question question : questions) {
            answer = new Answer();
            answer.setQuestionId(question.getId());
            answer.setUserId(user.getId());
            answer.setAnswer(answersString[index++]);
            answers.add(answer);
        }
        return answers;
    }
}
