package freelance.example.action;

import freelance.example.common.Answer;
import freelance.example.common.Question;
import freelance.example.common.User;
import freelance.example.common.exception.UserNotFoundException;
import freelance.example.manager.IAnswerManager;
import freelance.example.manager.IQuestionManager;
import freelance.example.manager.IUserManager;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.List;

public class Initializer implements ServletContextListener {

    /**
     * application context object (container)
     */
    private static ApplicationContext applicationContext;

    private static Logger logger = Logger.getLogger(Initializer.class);

    public static ServletContext context;

    static final String email = "seryozha.hovhannisyan@gmail.com";
    @Override
    public void contextInitialized(ServletContextEvent event) {
        try {
            logger.info("-- start application -- ");
            System.setProperty("file.encoding", "UTF-8");

            context = event.getServletContext();

            applicationContext = (ApplicationContext) context.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

            IUserManager userManager = (IUserManager) applicationContext.getBean("userManager");
            IQuestionManager questionManager = (IQuestionManager) applicationContext.getBean("questionManager");
            IAnswerManager answerManager = (IAnswerManager) applicationContext.getBean("answerManager");
            User user;
            // add user if not exist
            try {
                user = userManager.getUserByEmail(email);
            } catch (UserNotFoundException e){
                user = new User();
                user.setName("Seryozha");
                user.setSurname("Hovhannisyan");
                user.setEmail(email);
                userManager.addUser(user);
            }

            // add question/answers

            List<Question> questions = questionManager.getAll();
            if (questions == null || questions.size() == 0) {
                questions = DefaultDataUtil.generateQuestions();
                // insert questions
                questionManager.add(questions);
                questions = questionManager.getAll();
            }

            List<Answer> answers = answerManager.getUserAnswers(user.getId());
            if (answers == null || answers.size() == 0) {
                answers = DefaultDataUtil.generateAnswers(user, questions);
                // insert answers
                answerManager.add(answers);
            }

            logger.info("-- application started -- ");
        } catch (Exception e) {
            logger.error(e);
            throw new RuntimeException("unable intitilize application");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}