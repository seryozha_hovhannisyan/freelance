package freelance.example.action;

import com.opensymphony.xwork2.ActionSupport;
import freelance.example.common.Answer;
import freelance.example.common.User;
import freelance.example.common.exception.InternalErrorException;
import freelance.example.common.exception.UserNotFoundException;
import freelance.example.manager.IAnswerManager;
import freelance.example.manager.IUserManager;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by Seryozha on 20/1/2017.
 */
public class HomeAction extends ActionSupport {

    private List<Answer> answers;

    private User user;

    private IAnswerManager answerManager;

    private IUserManager userManager;

    static final String email = "seryozha.hovhannisyan@gmail.com";

    private static Logger logger = Logger.getLogger(HomeAction.class);

    public String renderHomePage(){
        try {
            user = userManager.getUserByEmail(email);
            answers = answerManager.getUserAnswers(user.getId());
        } catch (InternalErrorException e) {
            logger.error("Unexpected error");
            return ERROR;
        } catch (UserNotFoundException e) {
            logger.error("User not found["+email+"]");
            return ERROR;
        }
        return SUCCESS;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public User getUser() {
        return user;
    }

    public void setUserManager(IUserManager userManager) {
        this.userManager = userManager;
    }

    public void setAnswerManager(IAnswerManager answerManager) {
        this.answerManager = answerManager;
    }
}
